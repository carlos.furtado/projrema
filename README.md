# projrema

Projeto de Resistência dos Materiais

Devido a dificuldade de obtenção de dados para realizarmos o "Orçamento automatizado de sistemas solares", e por um dos integrantes do grupo estar cursando a disciplina de Resistência dos Materiais (REMA) e ser exigido a implementação computacional para solucionar um problema da disciplina, decidimos mudar o tema do nosso trabalho.
Cada um dos componentes do grupo foi responsável por realizar uma parte do projeto final, sendo:

- Dayanne Ferreira Dias: realizou o objeto responsável por armazenar os dados de entrada e fazer os métodos para converter os dados de saída.
- Carlos Eduardo Furtado Marquez da Cunha: realizou a construção da interface gráfica (tela para visualizar os dados de entrada e saída) como algumas validações.
- Fernando Soares Bastos: realizou a construção dos componentes para facilitar a construção da tela do programa.

Observação 1: por mera praticidade, optamos por apenas um integrante do grupo postar todo o trabalho final. 

Observação 2: recomendamos que o projeto seja executado utilizando o Intellij.
