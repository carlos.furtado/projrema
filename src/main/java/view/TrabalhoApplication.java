package view;

import javafx.application.Platform;
import javafx.event.ActionEvent;
import model.Sistema;

import java.text.DecimalFormat;
import java.text.NumberFormat;

public class TrabalhoApplication extends TrabalhoApplicationDesigner {

    public static void start(String[] args) {
        launch(args);
    }

    @Override
    protected void inicializarFormulario() {
        getBtnCalcular().setOnAction(this::clickCalcularSistema);
    }

    private void clickCalcularSistema(ActionEvent event) {
        if (validarDados()) {
            getEdtResposta().setText(clickCalcularSistema());
        }
        event.consume();
    }

    private String clickCalcularSistema() {
        Sistema sistema = getModel();

        if (sistema.getTensaoMaximaAB() / sistema.getTensaoAB() < sistema.getFatorSeguranca())
            Dialogo.atencao("Tensão no cabo AB está utrapassando o limite do fator de segurança.");

        if (sistema.getTensaoMaximaCD() / sistema.getTensaoCD() < sistema.getFatorSeguranca())
            Dialogo.atencao("Tensão no cabo CD está utrapassando o limite do fator de segurança.");

        if (sistema.getTensaoMaximaEF() / sistema.getTensaoEF() < sistema.getFatorSeguranca())
            Dialogo.atencao("Tensão no cabo EF está utrapassando o limite do fator de segurança.");

        return new StringBuilder()
                .append("Força Resultante:\n\n")
                .append("Fr = W * (L4 + L5 + L6)\n")
                .append("Fr = ").append(decimalFormat(sistema.getFr(), 3)).append(" N\n\n")
                .append("Reações de Apoio:\n\n")
                .append("Ve = (σef * Aef) ÷ Fs\n")
                .append("Ve = ").append(decimalFormat(sistema.getVe(), 3)).append(" N\n\n")
                .append("Vc = {[Fs * Fr * (L4 + L5 + L6)] - [2 * σef * Aef * (L4 + L5)]} ÷ (2 * Fs * L4)\n")
                .append("Vc = ").append(decimalFormat(sistema.getVc(), 3)).append(" N\n\n")
                .append("Va = Fr - Vc - Ve\n")
                .append("Va = ").append(decimalFormat(sistema.getVa(), 3)).append(" N\n\n")
                .append("Áreas:\n\n")
                .append("Acd = (Vc * L2 * Eef * Aef) ÷ (Ve * L3 * Ecd)\n")
                .append("Acd = ").append(decimalFormat(sistema.getAreaCD() * 1000000, 3)).append(" mm²\n\n")
                .append("Aab = (Va * L1 * Eef * Aef) ÷ (Ve * L3 * Eab)\n")
                .append("Aab = ").append(decimalFormat(sistema.getAreaAB() * 1000000, 3)).append(" mm²\n\n")
                .append("Deslocamentos:\n\n")
                .append("δb = (Va * L1) ÷ (Eab * Aab)\n")
                .append("δb = ").append(decimalFormat(sistema.getDeslocamentoB() * 1000, 3)).append(" mm\n\n")
                .append("δd = (Vc * L2) ÷ (Ecd * Acd)\n")
                .append("δd = ").append(decimalFormat(sistema.getDeslocamentoD() * 1000, 3)).append(" mm\n\n")
                .append("δf = (Ve * L3) ÷ (Eef * Aef)\n")
                .append("δf = ").append(decimalFormat(sistema.getDeslocamentoF() * 1000, 3)).append(" mm\n\n")
                .append("δg = δb = δd = δf\n")
                .append("δg = ").append(decimalFormat(sistema.getDeslocamentoB() * 1000, 3)).append(" mm\n\n")
                .append("Deformações no arame:\n\n")
                .append("εab = (Va) ÷ (Eab * Aab)\n")
                .append("εab = ").append(decimalFormat(sistema.getDeformacaoAB(), 3)).append(" mm/mm\n\n")
                .append("εcd = (Vc) ÷ (Ecd * Acd)\n")
                .append("εcd = ").append(decimalFormat(sistema.getDeformacaoCD(), 3)).append(" mm/mm\n\n")
                .append("εef = (Ve) ÷ (Eef * Aef)\n")
                .append("εef = ").append(decimalFormat(sistema.getDeformacaoEF(), 3)).append(" mm/mm\n\n")
                .append("Tensão dos arames:\n\n")
                .append("σeab = Va ÷ Aab\n")
                .append("σeab = ").append(decimalFormat(sistema.getTensaoAB(), 3)).append(" N/m\n\n")
                .append("σecd = Vc ÷ Acd\n")
                .append("σecd = ").append(decimalFormat(sistema.getTensaoCD(), 3)).append(" N/m\n\n")
                .append("σeef = Ve ÷ Aef\n")
                .append("σeef = ").append(decimalFormat(sistema.getTensaoEF(), 3)).append(" N/m\n\n")
                .toString();
    }

    public String decimalFormat(double valor, int decimalPlaces) {
        NumberFormat numberFormat = new DecimalFormat("#,##0." + formatDecimal(decimalPlaces));
        return numberFormat.format(valor);
    }

    private String formatDecimal(int decimalPlaces) {
        StringBuilder retorno = new StringBuilder();

        for (int i = 0; i < decimalPlaces; i++) {
            retorno.append("0");
        }

        return retorno.toString();
    }

    private Sistema getModel() {
        Sistema sistema = new Sistema();
        sistema.setValorCarga(getEdtValorCargaW().getValue() * 1000);
        sistema.setAreaEF(getEdtAreaEF().getValue() / 1000000);
        sistema.setFatorSeguranca(getEdtFatorSeguranca().getValue());
        sistema.setEscoamentoAB(getEdtLimiteEscoamentoBarraAB().getValue() * 1000000);
        sistema.setEscoamentoCD(getEdtLimiteEscoamentoBarraCD().getValue() * 1000000);
        sistema.setEscoamentoEF(getEdtLimiteEscoamentoBarraEF().getValue() * 1000000);
        sistema.setElasticidadeAB(getEdtModuloElasticidadeBarraAB().getValue() * 1000000000);
        sistema.setElasticidadeCD(getEdtModuloElasticidadeBarraCD().getValue() * 1000000000);
        sistema.setElasticidadeEF(getEdtModuloElasticidadeBarraEF().getValue() * 1000000000);
        sistema.setL1(getEdtL1().getValue());
        sistema.setL2(getEdtL2().getValue());
        sistema.setL3(getEdtL3().getValue());
        sistema.setL4(getEdtL4().getValue());
        sistema.setL5(getEdtL5().getValue());
        sistema.setL6(getEdtL6().getValue());
        return sistema;
    }

    private boolean validarDados() {
        if (getEdtValorCargaW().getValue() <= 0) {
            Dialogo.atencao("Informe o valor de carga distribuída");
            Platform.runLater(() -> getEdtValorCargaW().requestFocus());
            return false;
        }

        if (getEdtAreaEF().getValue() <= 0) {
            Dialogo.atencao("Informe a área da barra EF");
            Platform.runLater(() -> getEdtAreaEF().requestFocus());
            return false;
        }

        if (getEdtFatorSeguranca().getValue() < 1.5) {
            Dialogo.atencao("Fator de segurança deve ser maior ou igual a 1,5");
            Platform.runLater(() -> getEdtAreaEF().requestFocus());
            return false;
        }

        if (getEdtLimiteEscoamentoBarraAB().getValue() <= 0) {
            Dialogo.atencao("Informe o limite de escoamento da barra AB");
            Platform.runLater(() -> getEdtLimiteEscoamentoBarraAB().requestFocus());
            return false;
        }

        if (getEdtLimiteEscoamentoBarraCD().getValue() <= 0) {
            Dialogo.atencao("Informe o limite de escoamento da barra CD");
            Platform.runLater(() -> getEdtLimiteEscoamentoBarraCD().requestFocus());
            return false;
        }

        if (getEdtLimiteEscoamentoBarraEF().getValue() <= 0) {
            Dialogo.atencao("Informe o limite de escoamento da barra EF");
            Platform.runLater(() -> getEdtLimiteEscoamentoBarraEF().requestFocus());
            return false;
        }

        if (getEdtModuloElasticidadeBarraAB().getValue() <= 0) {
            Dialogo.atencao("Informe o módulo de elasticidade da barra AB");
            Platform.runLater(() -> getEdtModuloElasticidadeBarraAB().requestFocus());
            return false;
        }

        if (getEdtModuloElasticidadeBarraCD().getValue() <= 0) {
            Dialogo.atencao("Informe o módulo de elasticidade da barra CD");
            Platform.runLater(() -> getEdtModuloElasticidadeBarraCD().requestFocus());
            return false;
        }

        if (getEdtModuloElasticidadeBarraEF().getValue() <= 0) {
            Dialogo.atencao("Informe o módulo de elasticidade da barra EF");
            Platform.runLater(() -> getEdtModuloElasticidadeBarraEF().requestFocus());
            return false;
        }

        if (getEdtL1().getValue() <= 0) {
            Dialogo.atencao("Informe a distância AB");
            Platform.runLater(() -> getEdtL1().requestFocus());
            return false;
        }

        if (getEdtL2().getValue() <= 0) {
            Dialogo.atencao("Informe a distância CD");
            Platform.runLater(() -> getEdtL2().requestFocus());
            return false;
        }

        if (getEdtL3().getValue() <= 0) {
            Dialogo.atencao("Informe a distância EF");
            Platform.runLater(() -> getEdtL3().requestFocus());
            return false;
        }

        if (getEdtL4().getValue() <= 0) {
            Dialogo.atencao("Informe a distância BD");
            Platform.runLater(() -> getEdtL4().requestFocus());
            return false;
        }

        if (getEdtL5().getValue() <= 0) {
            Dialogo.atencao("Informe a distância DF");
            Platform.runLater(() -> getEdtL5().requestFocus());
            return false;
        }

        if (getEdtL6().getValue() <= 0) {
            Dialogo.atencao("Informe a distância FG");
            Platform.runLater(() -> getEdtL6().requestFocus());
            return false;
        }

        return true;
    }
}
