package view;

import javafx.application.Application;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

abstract class ApplicationBase extends Application {

    private AnchorPane componentPanel;
    private Stage stage;

    abstract void inicializarDesigner();

    @Override
    public void start(Stage stage) {
        this.stage = stage;

        componentPanel = new AnchorPane();
        stage.setScene(new Scene(componentPanel));

        inicializarDesigner();
        stage.show();
    }

    void setTitle(String title) {
        stage.setTitle(title);
    }

    void setSize(int width, int height) {
        stage.setWidth(width);
        stage.setHeight(height);
    }

    void addComponent(Node node) {
        componentPanel.getChildren().add(node);
    }
}
