package view;

import javafx.beans.value.ObservableValue;
import javafx.geometry.Pos;
import javafx.scene.control.TextField;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.NumberFormat;
import java.util.regex.Pattern;

public class DoubleTextField extends TextField {

    private static final DecimalFormatSymbols DECIMAL_FORMAT = DecimalFormatSymbols.getInstance();
    private static final String STR_EMPTY = "";
    private static final String DECIMAL_SEPARATOR = String.valueOf(DECIMAL_FORMAT.getDecimalSeparator());
    private static final String GROUPING_SEPARATOR = String.valueOf(DECIMAL_FORMAT.getGroupingSeparator());
    private static final String DOUBLE_SEPARATOR = ".";
    private static final String REGEX_DOUBLE = createRegexDouble();
    private static final NumberFormat NUMBER_FORMAT_DEFAULT = new DecimalFormat("#,##0.00########");

    public DoubleTextField() {
        textProperty().addListener(this::textChange);
        focusedProperty().addListener(this::eventFocusChange);
        setAlignment(Pos.CENTER_RIGHT);
    }

    private void eventFocusChange(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean focused) {
        if (!focused && !getText().isEmpty() && getText().matches(REGEX_DOUBLE))
            setText(NUMBER_FORMAT_DEFAULT.format(getValue()));
        else if (focused)
            setText(getText().replaceAll(Pattern.quote(GROUPING_SEPARATOR), STR_EMPTY));
    }

    private void textChange(ObservableValue<? extends String> observable, String oldValue, String newValue) {
        if (newValue == null || newValue.isEmpty() || newValue.matches(REGEX_DOUBLE))
            return;

        StringBuilder builder = new StringBuilder();
        char[] charArray = newValue.toCharArray();
        for (char c : charArray) {
            String str = String.valueOf(c);
            builder.append(str);

            if (!builder.toString().matches(REGEX_DOUBLE))
                builder.deleteCharAt(builder.length() - 1);
        }

        setText(builder.toString());
    }

    private static String createRegexDouble() {
        String quoteGrouping = Pattern.quote(GROUPING_SEPARATOR);
        return "(([0-9]{1,3}(" +
                quoteGrouping +
                "[0-9]{3})*|[0-9]*)[" +
                Pattern.quote(DECIMAL_SEPARATOR) +
                quoteGrouping +
                "])?[0-9]*";
    }

    public double getValue() {
        if (getText() == null || getText().isEmpty())
            return 0;

        String doubleStr = getText().replaceAll(Pattern.quote(GROUPING_SEPARATOR), STR_EMPTY);
        doubleStr = doubleStr.replaceAll(Pattern.quote(DECIMAL_SEPARATOR), DOUBLE_SEPARATOR);
        return Double.parseDouble(doubleStr);
    }
}
