package view;

import javafx.beans.value.ObservableValue;
import javafx.scene.control.TextField;

public class IntTextField extends TextField {

    private static final String REGEX_NO_NUMBERS = "[^\\d]";
    private static final String STR_EMPTY = "";

    public IntTextField() {
        textProperty().addListener(this::textChange);
    }

    private void textChange(ObservableValue<? extends String> observable, String oldValue, String newValue) {
        if (newValue == null || newValue.isEmpty())
            return;
        setText(newValue.replaceAll(REGEX_NO_NUMBERS, STR_EMPTY));
    }

    public int getInt() {
        return !getText().equals("") ? Integer.parseInt(getText()) : 0;
    }
}
