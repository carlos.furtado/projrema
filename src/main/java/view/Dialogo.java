package view;

import javafx.scene.control.Alert;

public class Dialogo {

    public static final String ATENCAO = "Atenção";

    public static void atencao(String mensagem) {
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle(ATENCAO);
        alert.setHeaderText(mensagem);
        alert.showAndWait();
    }
}
