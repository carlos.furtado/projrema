package view;

import javafx.geometry.HPos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.image.Image;
import javafx.scene.layout.*;

import java.util.Objects;

public abstract class TrabalhoApplicationDesigner extends ApplicationBase {

    public static final String TITULO = "R.E.M.A.";
    private DoubleTextField edtValorCargaW;
    private DoubleTextField edtAreaEF;
    private DoubleTextField edtFatorSeguranca;
    private DoubleTextField edtLimiteEscoamentoBarraAB;
    private DoubleTextField edtLimiteEscoamentoBarraCD;
    private DoubleTextField edtLimiteEscoamentoBarraEF;
    private DoubleTextField edtModuloElasticidadeBarraAB;
    private DoubleTextField edtModuloElasticidadeBarraCD;
    private DoubleTextField edtModuloElasticidadeBarraEF;
    private DoubleTextField edtL1;
    private DoubleTextField edtL2;
    private DoubleTextField edtL3;
    private DoubleTextField edtL4;
    private DoubleTextField edtL5;
    private DoubleTextField edtL6;
    private Button btnCalcular;
    private TextArea edtResposta;

    @Override
    final void inicializarDesigner() {
        setSize(1300, 817);
        setTitle(TITULO);

        GridPane gridPane = new GridPane();
        gridPane.setHgap(3);
        gridPane.setVgap(3);
        AnchorPane.setTopAnchor(gridPane, 10.0);
        AnchorPane.setLeftAnchor(gridPane, 10.0);
        AnchorPane.setRightAnchor(gridPane, 10.0);
        addComponent(gridPane);

        ColumnConstraints column0 = new ColumnConstraints();
        column0.setHalignment(HPos.RIGHT);
        column0.setHgrow(Priority.ALWAYS);
        ColumnConstraints column1 = new ColumnConstraints(150);
        gridPane.getColumnConstraints().addAll(column0, column1, column0, column1, column0, column1);

        Label rtlValorCargaW = new Label("Valor da carga distribuída (w em KN/m):");
        gridPane.add(rtlValorCargaW, 0, 0);

        edtValorCargaW = new DoubleTextField();
        gridPane.add(edtValorCargaW, 1, 0);

        Label rtlAreaEF = new Label("Área EF (Aef em mm²):");
        gridPane.add(rtlAreaEF, 2, 0);

        edtAreaEF = new DoubleTextField();
        gridPane.add(edtAreaEF, 3, 0);

        Label rtlFatorSeguranca = new Label("Fator de Segurança:");
        gridPane.add(rtlFatorSeguranca, 4, 0);

        edtFatorSeguranca = new DoubleTextField();
        gridPane.add(edtFatorSeguranca, 5, 0);

        Label rtlLimiteEscoamentoBarraAB = new Label("Limite de escoamento das barras (σab em MPa):");
        gridPane.add(rtlLimiteEscoamentoBarraAB, 0, 1);

        edtLimiteEscoamentoBarraAB = new DoubleTextField();
        gridPane.add(edtLimiteEscoamentoBarraAB, 1, 1);

        Label rtlLimiteEscoamentoBarraCD = new Label("Limite de escoamento das barras (σcd em MPa):");
        gridPane.add(rtlLimiteEscoamentoBarraCD, 2, 1);

        edtLimiteEscoamentoBarraCD = new DoubleTextField();
        gridPane.add(edtLimiteEscoamentoBarraCD, 3, 1);

        Label rtlLimiteEscoamentoBarraEF = new Label("Limite de escoamento das barras (σef em MPa):");
        gridPane.add(rtlLimiteEscoamentoBarraEF, 4, 1);

        edtLimiteEscoamentoBarraEF = new DoubleTextField();
        gridPane.add(edtLimiteEscoamentoBarraEF, 5, 1);

        Label rtlModuloElasticidadeBarraAB = new Label("Módulo de elasticidade das barras (Eab em GPa):");
        gridPane.add(rtlModuloElasticidadeBarraAB, 0, 2);

        edtModuloElasticidadeBarraAB = new DoubleTextField();
        gridPane.add(edtModuloElasticidadeBarraAB, 1, 2);

        Label rtlModuloElasticidadeBarraCD = new Label("Módulo de elasticidade das barras (Ecd em GPa):");
        gridPane.add(rtlModuloElasticidadeBarraCD, 2, 2);

        edtModuloElasticidadeBarraCD = new DoubleTextField();
        gridPane.add(edtModuloElasticidadeBarraCD, 3, 2);

        Label rtlModuloElasticidadeBarraEF = new Label("Módulo de elasticidade das barras (Eef em GPa):");
        gridPane.add(rtlModuloElasticidadeBarraEF, 4, 2);

        edtModuloElasticidadeBarraEF = new DoubleTextField();
        gridPane.add(edtModuloElasticidadeBarraEF, 5, 2);

        Label rtlL1 = new Label("Distância AB (L1 em m):");
        gridPane.add(rtlL1, 0, 3);

        edtL1 = new DoubleTextField();
        gridPane.add(edtL1, 1, 3);

        Label rtlL2 = new Label("Distância CD (L2 em m):");
        gridPane.add(rtlL2, 2, 3);

        edtL2 = new DoubleTextField();
        gridPane.add(edtL2, 3, 3);

        Label rtlL3 = new Label("Distância EF (L3 em m):");
        gridPane.add(rtlL3, 4, 3);

        edtL3 = new DoubleTextField();
        gridPane.add(edtL3, 5, 3);

        Label rtlL4 = new Label("Distância BD (L4 em m):");
        gridPane.add(rtlL4, 0, 4);

        edtL4 = new DoubleTextField();
        gridPane.add(edtL4, 1, 4);

        Label rtlL5 = new Label("Distância DF (L5 em m):");
        gridPane.add(rtlL5, 2, 4);

        edtL5 = new DoubleTextField();
        gridPane.add(edtL5, 3, 4);

        Label rtlL6 = new Label("Distância FG (L6 em m):");
        gridPane.add(rtlL6, 4, 4);

        edtL6 = new DoubleTextField();
        gridPane.add(edtL6, 5, 4);

        Label rtlEsquema = new Label("Esquema");
        AnchorPane.setTopAnchor(rtlEsquema, 185.0);
        AnchorPane.setLeftAnchor(rtlEsquema, 10.0);
        addComponent(rtlEsquema);

        AnchorPane plnImagemEsquema = new AnchorPane();
        plnImagemEsquema.setPrefHeight(270);
        plnImagemEsquema.setPrefWidth(768);
        AnchorPane.setTopAnchor(plnImagemEsquema, 205.0);
        AnchorPane.setLeftAnchor(plnImagemEsquema, 10.0);
        addComponent(plnImagemEsquema);

        Image imageEsquema = new Image(Objects.requireNonNull(getClass().getResourceAsStream("/esquema.jpg")));
        plnImagemEsquema.setBackground(new Background(new BackgroundImage(imageEsquema, BackgroundRepeat.NO_REPEAT, BackgroundRepeat.NO_REPEAT, BackgroundPosition.CENTER, BackgroundSize.DEFAULT)));

        Label rtlMateriaisSugeridos = new Label("Materiais Sugeridos");
        AnchorPane.setTopAnchor(rtlMateriaisSugeridos, 479.0);
        AnchorPane.setLeftAnchor(rtlMateriaisSugeridos, 10.0);
        addComponent(rtlMateriaisSugeridos);

        AnchorPane plnImagemMateriaisSugeridos = new AnchorPane();
        plnImagemMateriaisSugeridos.setPrefHeight(270);
        plnImagemMateriaisSugeridos.setPrefWidth(768);
        AnchorPane.setTopAnchor(plnImagemMateriaisSugeridos, 499.0);
        AnchorPane.setLeftAnchor(plnImagemMateriaisSugeridos, 10.0);
        addComponent(plnImagemMateriaisSugeridos);

        Image imageMateriaisSugeridos = new Image(Objects.requireNonNull(getClass().getResourceAsStream("/materiais-sugeridos.jpg")));
        plnImagemMateriaisSugeridos.setBackground(new Background(new BackgroundImage(imageMateriaisSugeridos, BackgroundRepeat.NO_REPEAT, BackgroundRepeat.NO_REPEAT, BackgroundPosition.CENTER, BackgroundSize.DEFAULT)));

        btnCalcular = new Button("Calcular Sistema");
        AnchorPane.setTopAnchor(btnCalcular, 155.0);
        AnchorPane.setRightAnchor(btnCalcular, 10.0);
        addComponent(btnCalcular);

        Label rtlResposta = new Label("Resposta");
        AnchorPane.setTopAnchor(rtlResposta, 185.0);
        AnchorPane.setLeftAnchor(rtlResposta, 783.0);
        addComponent(rtlResposta);

        edtResposta = new TextArea();
        edtResposta.setEditable(false);
        AnchorPane.setTopAnchor(edtResposta, 205.0);
        AnchorPane.setLeftAnchor(edtResposta, 783.0);
        AnchorPane.setRightAnchor(edtResposta, 10.0);
        AnchorPane.setBottomAnchor(edtResposta, 10.0);
        addComponent(edtResposta);

        inicializarFormulario();
    }

    protected abstract void inicializarFormulario();

    DoubleTextField getEdtValorCargaW() {
        return edtValorCargaW;
    }

    DoubleTextField getEdtAreaEF() {
        return edtAreaEF;
    }

    DoubleTextField getEdtFatorSeguranca() {
        return edtFatorSeguranca;
    }

    DoubleTextField getEdtLimiteEscoamentoBarraAB() {
        return edtLimiteEscoamentoBarraAB;
    }

    DoubleTextField getEdtLimiteEscoamentoBarraCD() {
        return edtLimiteEscoamentoBarraCD;
    }

    DoubleTextField getEdtLimiteEscoamentoBarraEF() {
        return edtLimiteEscoamentoBarraEF;
    }

    DoubleTextField getEdtModuloElasticidadeBarraAB() {
        return edtModuloElasticidadeBarraAB;
    }

    DoubleTextField getEdtModuloElasticidadeBarraCD() {
        return edtModuloElasticidadeBarraCD;
    }

    DoubleTextField getEdtModuloElasticidadeBarraEF() {
        return edtModuloElasticidadeBarraEF;
    }

    DoubleTextField getEdtL1() {
        return edtL1;
    }

    DoubleTextField getEdtL2() {
        return edtL2;
    }

    DoubleTextField getEdtL3() {
        return edtL3;
    }

    DoubleTextField getEdtL4() {
        return edtL4;
    }

    DoubleTextField getEdtL5() {
        return edtL5;
    }

    DoubleTextField getEdtL6() {
        return edtL6;
    }

    Button getBtnCalcular() {
        return btnCalcular;
    }

    TextArea getEdtResposta() {
        return edtResposta;
    }
}
