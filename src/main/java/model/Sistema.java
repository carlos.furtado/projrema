package model;

public class Sistema {

    private double valorCarga;
    private double areaEF;
    private double fatorSeguranca;
    private double escoamentoAB;
    private double escoamentoCD;
    private double escoamentoEF;
    private double elasticidadeAB;
    private double elasticidadeCD;
    private double elasticidadeEF;
    private double l1;
    private double l2;
    private double l3;
    private double l4;
    private double l5;
    private double l6;

    public void setValorCarga(double valorCarga) {
        this.valorCarga = valorCarga;
    }

    public void setAreaEF(double areaEF) {
        this.areaEF = areaEF;
    }

    public void setFatorSeguranca(double fatorSeguranca) {
        this.fatorSeguranca = fatorSeguranca;
    }

    public void setEscoamentoAB(double escoamentoAB) {
        this.escoamentoAB = escoamentoAB;
    }

    public void setEscoamentoCD(double escoamentoCD) {
        this.escoamentoCD = escoamentoCD;
    }

    public void setEscoamentoEF(double escoamentoEF) {
        this.escoamentoEF = escoamentoEF;
    }

    public void setElasticidadeAB(double elasticidadeAB) {
        this.elasticidadeAB = elasticidadeAB;
    }

    public void setElasticidadeCD(double elasticidadeCD) {
        this.elasticidadeCD = elasticidadeCD;
    }

    public void setElasticidadeEF(double elasticidadeEF) {
        this.elasticidadeEF = elasticidadeEF;
    }

    public void setL1(double l1) {
        this.l1 = l1;
    }

    public void setL2(double l2) {
        this.l2 = l2;
    }

    public void setL3(double l3) {
        this.l3 = l3;
    }

    public void setL4(double l4) {
        this.l4 = l4;
    }

    public void setL5(double l5) {
        this.l5 = l5;
    }

    public void setL6(double l6) {
        this.l6 = l6;
    }

    public double getFatorSeguranca() {
        return fatorSeguranca;
    }

    public double getFr() {
        return valorCarga * (l4 + l5 + l6);
    }

    public double getVe() {
        return (escoamentoEF * areaEF) / fatorSeguranca;
    }

    public double getVc() {
        return ((fatorSeguranca * getFr() * (l4 + l5 + l6)) - (2 * escoamentoEF * areaEF * (l4 + l5))) / (2 * fatorSeguranca * l4);
    }

    public double getVa() {
        return getFr() - getVc() - getVe();
    }

    public double getAreaAB() {
        return (getVa() * l1 * elasticidadeEF * areaEF) / (getVe() * l3 * elasticidadeAB);
    }

    public double getAreaCD() {
        return (getVc() * l2 * elasticidadeEF * areaEF) / (getVe() * l3 * elasticidadeCD);
    }

    public double getDeslocamentoB() {
        return (getVa() * l1) / (elasticidadeAB * getAreaAB());
    }

    public double getDeslocamentoD() {
        return (getVc() * l2) / (elasticidadeCD * getAreaCD());
    }

    public double getDeslocamentoF() {
        return (getVe() * l3) / (elasticidadeEF * areaEF);
    }

    public double getDeformacaoAB() {
        return getVa() / (elasticidadeAB * getAreaAB());
    }

    public double getDeformacaoCD() {
        return getVc() / (elasticidadeCD * getAreaCD());
    }

    public double getDeformacaoEF() {
        return getVe() / (elasticidadeEF * areaEF);
    }

    public double getTensaoAB() {
        return getVa() / getAreaAB();
    }

    public double getTensaoCD() {
        return getVc() / getAreaCD();
    }

    public double getTensaoEF() {
        return getVe() / areaEF;
    }

    public double getTensaoMaximaAB() {
        return escoamentoAB;
    }

    public double getTensaoMaximaCD() {
        return escoamentoCD;
    }

    public double getTensaoMaximaEF() {
        return escoamentoEF;
    }

    public double getAreaMinimaAB() {
        return (fatorSeguranca * getVa()) / getTensaoMaximaAB();
    }

    public double getAreaMinimaCD() {
        return (fatorSeguranca * getVc()) / getTensaoMaximaCD();
    }

    public double getAreaMinimaEF() {
        return (fatorSeguranca * getVe()) / getTensaoMaximaEF();
    }
}
